
from products.views import (
    list_all_products,
    list_vendors,
    list_categories,
    show_category,
    show_product,
    show_vendor,
    edit_product,
    edit_category,
    edit_vendor,
    delete_product,
    delete_category,
    delete_vendor,
    create_product,
    create_vendor,
    create_category,
    home_page,
    switch_cat,
)
from django.urls import path

urlpatterns = [
    path("home/", home_page, name="home_page"),
    path("switch/<int:id>", switch_cat, name="switch_cat"),
    path("", list_all_products, name="list_all_products"),
    path("create/", create_product, name="create_product"),
    path("detail/<int:id>", show_product, name="show_product"),
    path("edit/<int:id>", edit_product, name="edit_product"),
    path("delete/<int:id>", delete_product, name="delete_product"),
    path("vendors/", list_vendors, name="list_vendors"),
    path("vendors/create/", create_vendor, name="create_vendor"),
    path("vendors/detail/<int:id>", show_vendor, name="show_vendor"),
    path("vendors/edit/<int:id>", edit_vendor, name="edit_vendor"),
    path("vendors/delete/<int:id>", delete_vendor, name="delete_vendor"),
    path("categories/", list_categories, name="list_categories"),
    path("categories/create/", create_category, name="create_category"),
    path("categories/detail/<int:id>", show_category, name="show_category"),
    path("categories/edit/<int:id>", edit_category, name="edit_category"),
    path("categories/delete/<int:id>", delete_category, name="delete_category"),

]
