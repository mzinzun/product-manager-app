from django.db import models

# Create your models here.


class Product(models.Model):
    name = models.CharField(max_length=200)
    sku = models.CharField(max_length=50)
    description = models.TextField()
    count = models.IntegerField()
    price = models.DecimalField(max_digits=6, decimal_places=2)
    date_added = models.DateField()
    img_url = models.ImageField(upload_to='images/', null=True)
    vendor = models.ForeignKey(
        'Vendor',
        related_name='products',
        on_delete=models.CASCADE,
        null=True

    )
    category = models.ForeignKey(
        'Category',
        related_name='products',
        on_delete=models.CASCADE,
        null=True,
    )


class Category(models.Model):
    category_name = models.CharField(max_length=100)
    cat_img = models.ImageField(upload_to='images/', null=True)
    date_added = models.DateField()

    def __str__(self):
        return self.category_name


class Vendor(models.Model):
    vendor_name = models.CharField(max_length=200)
    address = models.TextField(max_length=400)
    email = models.EmailField()
    contact = models.CharField(max_length=200)
    date_added = models.DateField()

    def __str__(self):
        return self.vendor_name
