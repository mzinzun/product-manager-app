from django.forms import ModelForm
from products.models import Product, Category, Vendor


class ProductForm(ModelForm):
    class Meta:
        model = Product
        fields = [
            "name",
            "sku",
            "description",
            "count",
            "price",
            "date_added",
            "img_url",
            "vendor",
            "category",
        ]


class CategoryForm(ModelForm):
    class Meta:
        model = Category
        fields = [
            "category_name",
            "cat_img",
            "date_added",
        ]


class VendorForm(ModelForm):
    class Meta:
        model = Vendor
        fields = [
            "vendor_name",
            "address",
            "contact",
            "email",
            "date_added"
        ]
