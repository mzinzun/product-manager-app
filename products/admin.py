from django.contrib import admin
from products.models import Product, Category, Vendor

# Register your models here.


@admin.register(Product)
class Product(admin.ModelAdmin):
    list_display = (
        "name",
        "sku",
        "description",
    )


@admin.register(Category)
class Category(admin.ModelAdmin):
    list_display = (
        "category_name",
    )


@admin.register(Vendor)
class Vendor(admin.ModelAdmin):
    list_display = (
        "vendor_name",
        "contact",
    )
