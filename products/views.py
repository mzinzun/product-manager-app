from django.shortcuts import render, redirect, get_object_or_404
from products.models import Product, Vendor, Category
from products.forms import ProductForm, CategoryForm, VendorForm
from django.contrib.auth.decorators import login_required

# Create your views here.


def home_page(request):
    catagories = Category.objects.all()
    context = {
        "catagories": catagories,
        "home": True,
    }

    return render(request, 'index.html', context)


def switch_cat(request, id):
    print('name coming in:', id)
    catagories = Category.objects.all()
    products = Product.objects.filter(category=id)
    # could also use:
    # category = Category.objects.get(id=id)
    # products = category.products.all()
    context = {
        "catagories": catagories,
        "products": products,
        "home": True,
    }
    return render(request, 'index.html', context)


@login_required
def list_all_products(request):
    products = Product.objects.all()
    context = {
        "products": products,
    }
    return render(request, "products/productsList.html", context)


@login_required
def list_vendors(request):
    vendors = Vendor.objects.all()
    context = {
        "vendors": vendors,
    }
    return render(request, "vendors/vendorList.html", context)


@login_required
def list_categories(request):
    categories = Category.objects.all()
    context = {
        "categories": categories,
    }
    return render(request, "categories/categoryList.html", context)


@login_required
def show_product(request, id):
    product = Product.objects.get(id=id)
    context = {
        "product": product
    }
    return render(request, 'products/showProduct.html', context)


@login_required
def show_category(request, id):
    category = Category.objects.get(id=id)
    context = {
        "category": category
    }
    return render(request, 'categories/showCategory.html', context)


@login_required
def show_vendor(request, id):
    vendor = get_object_or_404(Vendor, id=id)
    context = {
        "vendor": vendor
    }
    return render(request, 'vendors/showVendor.html', context)


@login_required
def edit_product(request, id):
    product = Product.objects.get(id=id)
    if request.method == "POST":
        form = ProductForm(request.POST, request.FILES, instance=product)
        if form.is_valid():
            form.save()
            return redirect("show_product", id=id)
    else:
        form = ProductForm(instance=product)
    context = {
        "product": product,
        "form": form
    }
    return render(request, 'products/editProduct.html', context)


@login_required
def edit_vendor(request, id):
    vendor = Vendor.objects.get(id=id)
    if request.method == "POST":
        form = VendorForm(request.POST, instance=vendor)
        if form.is_valid():
            form.save()
            return redirect("show_vendor", id=id)
    else:
        form = VendorForm(instance=vendor)
    context = {
        "vendor": vendor,
        "form": form
    }
    return render(request, 'vendors/editVendor.html', context)


@login_required
def edit_category(request, id):
    category = Category.objects.get(id=id)
    if request.method == "POST":
        form = CategoryForm(request.POST, request.FILES, instance=category)
        if form.is_valid():
            form.save()
            return redirect("show_category", id=id)
    else:
        form = CategoryForm(instance=category)
    context = {
        "category": category,
        "form": form
    }
    return render(request, 'categories/editCategory.html', context)


@login_required
def delete_product(request, id):
    product = get_object_or_404(Product, id=id)
    product.delete()
    return redirect('list_all_products')


@login_required
def delete_vendor(request, id):
    vendor = get_object_or_404(Vendor, id=id)
    products = Product.objects.filter(vendor=vendor.vendor_name)
    if len(products) > 0:
        print(f'vendor has {len(products)} assigned')
    else:
        vendor.delete()
    return redirect('list_vendors')


@login_required
def delete_category(request, id):
    category = get_object_or_404(Category, id=id)
    category.delete()
    return redirect('list_categories')


@login_required
def create_product(request):
    if request.method == "POST":
        form = ProductForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('list_all_products')
    else:
        form = ProductForm()
    context = {
        "form": form
    }
    return render(request, 'products/createProduct.html', context)


@login_required
def create_vendor(request):
    if request.method == "POST":
        form = VendorForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('list_vendors')
    else:
        form = VendorForm()
    context = {
        "form": form
    }
    return render(request, 'vendors/createVendor.html', context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('list_categories')
    else:
        form = CategoryForm()
    context = {
        "form": form
    }
    return render(request, 'categories/createCategory.html', context)
