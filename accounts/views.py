from django.shortcuts import render,redirect
from accounts.forms import LoginForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

# Create your views here.


def login_form(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            password = form.cleaned_data["password"]
            username = form.cleaned_data["username"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("list_all_products")
    else:
        form = LoginForm()
    context = {
        "form": form,
        "home": True,
    }
    return render(request, "userLogin.html", context)


def logout_user(request):
    logout(request)
    return redirect("home_page")
