# Ram
## image: ram1
https://www.dell.com/en-us/shop/dell-memory-upgrade-32gb-2rx8-ddr5-sodimm-4800mhz/apd/ab949335/memory?gacd=9684992-1102-5761040-266906002-0&dgc=ST&SA360CID=71700000109860316&gad=1&gclid=CjwKCAjwjOunBhB4EiwA94JWsNE6gkDbJTVroz1GtYeZ4SsqrsSkdy6Txfx_9eYLbQA5LMlaHfmtUBoCKTQQAvD_BwE&gclsrc=aw.ds#:~:text=SODIMM%204800%20MHz-,Dell%20Memory%20Upgrade%20%2D%2032%20GB%20%2D%202RX8%20DDR5%20SODIMM%204800%20MHz,Apply%20Now,-Dell%20Rewards%20Earn

## image: VENG_LPX_BLK_01

VENGEANCE® LPX 64GB (2 x 32GB) DDR4
DRAM 3200MHz C16 Memory Kit - Black
VENGEANCE LPX memory is designed for high-performance overclocking.
The heatspreader is made of pure aluminum for faster heat dissipation, and the eight-layer PCB helps manage heat and provides superior overclocking headroom.

## image ram3
Kingston FURY Beast 64GB (2 x 32GB) 288-Pin DDR5 6000 (PC5 48000) Memory
Enhanced RGB lighting with new heat spreader design
Intel XMP 3.0 Certified
Patented Kingston FURY Infrared Sync Technology
Improved stability for overclocking
DDR5 6000 (PC5 48000)
CAS Latency 40
Voltage 1.35V

## image: 91C96tnFwLL._AC_SX679_PIbundle-2,TopRight,0,0_SH20_
Silicon Power DDR4 32GB (2x16GB)
Turbine 3200MHz (PC4 25600) 288-pin CL16 1.35V UDIMM Desktop Memory Module RAM - Low Voltage (SP032GXLZU320BDA)

# Monitors
## image: s3422dwg-xfp-01-bk
Dell 34 Curved Gaming Monitor – S3422DWG
 4.6 (1127) Ask a Question
34" curved WQHD monitor with VESA Display HDR 400, a 144Hz refresh rate providing a truly immersive gaming experience.

## image: 6394746_sd
Insignia™ 24" Class N10 Series LED HD TV
Display Type
LED
Resolution
HD (720p)
Screen Size Class
24 inches
High Dynamic Range (HDR)
No
LED Panel Type
Standard LED
Backlight Type
Direct Lit
Refresh Rate
60Hz
Product Height With Stand
14.3 inches

## image: Dual_V27i_G5_Mon_Kit_573x430
Dual V27i G5 27" Monitor Bundle
Viewable image area: 27-inch diagonally measured
• Panel type: IPS
• Aspect ratio: 16:9
• Resolution: Full HD (1920 x 1080)
• Connectivity: 1 HDMI 1.4; 1 DisplayPort™ 1.2; 1 VGA

## image: Deco Gear 30" Curved Professional Monitor
Deco Gear 30" Curved Professional Monitor
2560x1080, 200 HZ, 1ms MPRT, High Color Accuracy

# Harddrives

## image: 6425021_sd
Seagate - Barracuda 8TB Hard Drive - Desktops
Specifications
Storage Capacity
8000 gigabytes
Storage Drive Type
HDD
Form Factor
3.5 in.
Interface(s)
SATA

## image: wd-red-plus-sata-3-5-hdd-3tb.png.wdthumb.1280.1280
WD Red Plus NAS Hard Drive 3.5-Inch
from Western Digital
Ideal for Home Offices, Power Users, Small to Medium Businesses and Consumer/Commercial NAS systems
For RAID-optimized NAS systems with up to 8 bays
Rated for 180TB/year workload1 and 1M hours MTBF

## image: MZ-V7S2T0BW_001
970 EVO Plus NVMe® M.2 SSD 2TB
The ultimate in performance, upgraded. Faster than the 970 EVO, the 970 EVO Plus is powered by the latest V-NAND technology and firmware optimization. It maximizes the potential of NVMe® bandwidth for unbeatable computing. Comes in capacities of up to 2TB, with reliability of up to 1,200 TBW.

## image:black-ssd
The Portable SSD is a cutting-edge storage solution that combines blazing-fast performance, compact design, and unmatched reliability.
Designed to meet the needs of professionals and on-the-go users, this sleek and compact SSD is the ideal choice for anyone seeking to enhance their storage capabilities without compromising on speed or durability. With storage sizes up to 64TB. Save and transfer massive files in a matter of seconds.

# Input Divices
## input: 205173_o01
Logitech K120 Wired Keyboard
For Windows, USB Plug-and-Play, Full-Size, Spill-Resistant, Curved Space Bar, Compatible with PC, Laptop - Black

## image: inputCombo
Logitech MX Keys Combo for Business Gen 2 in Graphite
$219.00
M-Audio Keystation 49 MK3 49-key Keyboard Controller
Razer Tartarus Pro Gaming Keypad: Analog-Optical Key Switches - 32 Programmable Keys, Macros - Customizable Chroma RGB Lighting - Variable Key Press
Kensington Pro Fit Ergo Wired Keyboard

## image: 71HzellzEYL._AC_SX679_
Logitech G13 Programmable Gameboard with LCD DisplayLogitech G13 Programmable Gameboard with LCD Display
Razer Tartarus Pro Gaming Keypad: Analog-Optical Key Switches - 32 Programmable Keys, Macros - Customizable Chroma RGB Lighting - Variable Key Press Pressure Sensitivity - Classic Black

## image: PROv2withlogotransparent1200x1200_688x688
Shuttle Pro V2 multi-media editing
The ShuttlePro V2 is the premier multi-media editing tool. Whether you are working in video, audio, photography, graphic design, or any creative setting the Shuttle can help maximize productivity and allow you to focus on what matters, creating.

COPY accounts accounts
COPY media media
COPY product_manager product_manager
COPY products products
COPY manage.py manage.py
